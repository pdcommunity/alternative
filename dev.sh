(cd front; webpack --watch --mode development) &
P1=$!

rm public -r
mkdir public
ln -s "$PWD/front/404.html" public/404.html
ln -s "$PWD/front/404.html" public/index.html
ln -s "$PWD/front/sw.js" public/sw.js
ln -s "$PWD/front/dist" public/dist
npx yaml2json front/src/data/data.yaml > public/dist/data.json
http-server public &
P2=$!

wait $P1 $P2
