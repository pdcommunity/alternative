rm public -r
mkdir public
cd front
webpack --mode production
mv dist ../public
cp 404.html ../public
cp 404.html ../public/index.html
cp sw.js ../public/
npx yaml2json src/data/data.yaml > ../public/dist/data.json
