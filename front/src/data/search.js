/* eslint-disable toplevel/no-toplevel-side-effect */
/* eslint-disable toplevel/no-toplevel-let */
import { levenshtein } from "./levenshtein";

let data = [];
let names = [];

/**
 * @type {Map<string, string>}
 */
let owner = new Map();

const main = async () => {
  try {
    data = await (await fetch("/dist/data.json")).json();
    names = data.map(x => [x.name, ...x.othername]).flat();
    owner = (()=>{
      const d = new Map();
      data.forEach((e) => {
        d.set(e.name, e.name);
        e.othername.forEach(f => {
          d.set(f, e.name);
        });
      });
      return d;
    })();
  } catch (e) {
    console.log(e);
  }
};

main();

const cmp = (q) => (a, b) => {
  return levenshtein(q, a.slice(0, q.length)) - levenshtein(q, b.slice(0, q.length));
};

export const search = (q) => {
  names.sort(cmp(q));
  const g = {};
  const r = [];
  for (let i = 0; i < names.length; i += 1) {
    const x = owner.get(names[i]);
    if (!g[x]) {
      // eslint-disable-next-line immutable/no-mutation
      g[x] = true;
      r.push(x);
      if (r.length === 6) return r;
    }
  }
  return r;
};
