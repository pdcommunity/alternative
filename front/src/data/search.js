import data from "./data.yaml";
import { levenshtein } from "./levenshtein";

const names = data.map(x => [x.name, ...x.othername]).flat();

const owner = (()=>{
  const d = {};
  data.forEach((e) => {
    // eslint-disable-next-line immutable/no-mutation
    d[e.name] = e.name;
    e.othername.forEach(f => {
      // eslint-disable-next-line immutable/no-mutation
      d[f] = e.name;
    });
  });
  return d;
})();

const cmp = (q) => (a, b) => {
  return levenshtein(q, a.slice(0, q.length)) - levenshtein(q, b.slice(0, q.length));
};

export const search = (q) => {
  console.log(names);
  console.log(q);
  names.sort(cmp(q));
  const g = {};
  const r = [];
  for (let i = 0; i < names.length; i += 1) {
    const x = owner[names[i]];
    if (!g[x]) {
      // eslint-disable-next-line immutable/no-mutation
      g[x] = true;
      r.push(x);
      if (r.length === 6) return r;
    }
  }
  return r;
};
