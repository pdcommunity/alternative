/* eslint-disable no-unused-vars */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { Home } from "./home/Home";
import { CandidatePage } from "./home/CandidatePage";
import { InfoRouter } from "./info/InfoRouter";
import { ErrorBoundary } from "./template/ErrorBoundary";

export default function App() {
  return (
    <Router>
      <ErrorBoundary>
        <Switch>
          <Route path="/info">
            <InfoRouter />
          </Route>
          <Route path="/:title">
            <CandidatePage />
          </Route>
          <Route path="/" exact>
            <Home />
          </Route>
        </Switch>
      </ErrorBoundary>
    </Router>
  );
}
