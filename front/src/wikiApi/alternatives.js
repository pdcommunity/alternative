import wiki from "wikijs";
import TurndownService from 'turndown';

const ts = new TurndownService({
  headingStyle: 'atx', linkStyle: 'referenced', linkReferenceStyle: 'shortcut',
});

const imageRegx = /!\[\]\([^)]*\)/g;

const getHtml = async (title) => {
  try {
    const wikiPage = await wiki({ apiUrl: 'https://index.pdcommunity.ir/w/api.php' })
      .page(title);
    return wikiPage.html();
  } catch (e) {
    return '404';
  }
};

const removeMarkdown = (x) => {
  return x.replace(/\[/g, '').replace(/\]/g, '').trim();
};

export const getAlt = async (title) => {
  const html = await getHtml(title);
  if (html === '404') {
    return {
      ok: false,
      reason: 'page not found',
    };
  }
  const r = ts.turndown(html).replace(imageRegx, '')
    .split('\n')
    .filter(x => x !== '');
  for (;;) {
    const x = r.pop();
    if (x[0] !== '[') {
      r.push(x);
      break;
    }
  }
  const description = removeMarkdown(
    html.slice(html.indexOf(title) + title.length, html.indexOf('است')),
  );
  const alt = [];
  let whyNot = '';
  let started = 0;
  let curObj;
  for (let i = 0; i < r.length; i += 1) {
    const x = r[i];
    if (x.indexOf('#') !== -1) {
      if (x.indexOf('# جایگزین ها') !== -1) started = 1;
      if (x.indexOf('# چرا') !== -1) started = 2;
    } else if (started === 1) {
      if (x[0] === '*') {
        if (curObj) alt.push(curObj);
        const t = x.slice(1).split(':');
        curObj = {
          name: removeMarkdown(t[0]),
          description: removeMarkdown(t[1]),
          comments: [],
        };
      } else if (x.slice(0, 5) === '    *') {
        const t = x.split(':');
        curObj.comments.push({
          tag: t[0].replace(/\*/g, '').trim(),
          description: t[1].trim(),
        });
      } else if (x[0] === 'ج') {
        curObj = {
          redirect: true,
          to: removeMarkdown(x).slice(12, -11),
        };
      }
    } else if (started === 2) {
      whyNot += x;
    }
  }
  alt.push(curObj);
  return {
    ok: true,
    alt,
    whyNot,
    description,
  };
};
