import { useEffect, useState } from "react";
import { getAlt } from "./alternatives";

const cache = {};

const getCache = (title) => {
  if (cache[title]) return cache[title];
  // eslint-disable-next-line immutable/no-mutation
  cache[title] = {
    loading: true,
    ok: false,
  };
  return cache[title];
};

const getCachedAlt = async (title) => {
  if (!getCache(title).loading) {
    return cache[title];
  }
  const x = await getAlt(title);
  // eslint-disable-next-line immutable/no-mutation
  cache[title] = {
    ...x,
    loading: false,
  };
  return cache[title];
};

const subscribe = async (title, setter) => {
  if (!cache[title].loading) {
    return;
  }
  console.log(title);
  const x = await getCachedAlt(title);
  setter(x);
  if (x.alt.length === 1 && x.alt[0].redirect) {
    // nemishe mutate nakard chon bayad aslie avaz she
    // eslint-disable-next-line immutable/no-mutation
    x.alt = (await getCachedAlt(x.alt[0].to)).alt;
    // in kesafat kari baraye mutate kardane
    setter({ ...x });
  }
};

export const useWikiApi = (title) => {
  const [s, setS] = useState(getCache(title));
  useEffect(() => {
    subscribe(title, setS);
  });
  return s;
};
