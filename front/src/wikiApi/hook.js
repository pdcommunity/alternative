import { useEffect, useState } from "react";
import { getAlt } from "./alternatives";

const cache = {};

const getCache = (title) => {
  if (cache[title]) return cache[title];
  // eslint-disable-next-line immutable/no-mutation
  cache[title] = {
    loading: true,
    ok: false,
  };
  return cache[title];
};

const subscribe = async (title, setter) => {
  if (!cache[title].loading) {
    return;
  }
  // eslint-disable-next-line immutable/no-mutation
  cache[title] = {
    ...await getAlt(title),
    loading: false,
  };
  setter(cache[title]);
};

export const useWikiApi = (title) => {
  const [s, setS] = useState(getCache(title));
  useEffect(() => {
    subscribe(title, setS);
  });
  return s;
};
