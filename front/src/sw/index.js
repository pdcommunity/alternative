import Swal from "sweetalert2";

export const main = async () => {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js', {
      scope: '/',
    });
    navigator.serviceWorker.addEventListener('message', async ({ data }) => {
      if (data.type === 'activated') {
        const result = await Swal.fire({
          icon: 'success',
          position: 'bottom-end',
          title: `نسخه ${data.version} نصب شد`,
          html: 'به صورت آفلاین می توانید از جایگزین یاب استفاده کنید',
          timer: 5000,
          confirmButtonText: 'حله',
          cancelButtonText: 'اطلاعات بیشتر',
          showCancelButton: true,
          timerProgressBar: true,
        });
        if (result.dismiss === Swal.DismissReason.cancel) {
          // eslint-disable-next-line immutable/no-mutation
          window.location = '/info/about';
        }
      }
    });
  }
};
