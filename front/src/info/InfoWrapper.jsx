import React from "react";
import { DefaultTemplate } from "../template/DefaultTemplate";

export const InfoWrapper = ({ children }) => {
  return <DefaultTemplate>
    <div className="text-white">
      {children}
    </div>
  </DefaultTemplate>;
};
