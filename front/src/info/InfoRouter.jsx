import React from "react";
import {
  useRouteMatch,
  Switch,
  Route,
} from "react-router-dom";
import { AboutThis } from "./AboutThis";
import { NotFound } from "../template/NotFound";
import { HelpRouter } from "./help/HelpRouter";

export const InfoRouter = () => {
  const { url } = useRouteMatch();
  return <Switch>
    <Route path={`${url}/about`}>
      <AboutThis/>
    </Route>
    <Route path={`${url}/help`}>
      <HelpRouter/>
    </Route>
    <Route path="*">
      <NotFound/>
    </Route>
  </Switch>;
};
