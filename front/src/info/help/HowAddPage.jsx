import React from "react";
import { InfoWrapper } from "../InfoWrapper";
import { Link } from "react-router-dom";

export const HowAddPage = () => {
  return <InfoWrapper>
    <h1>راهنمای افزودن صفحه یک داده انحصاری</h1>
    <ol>
      <li>
        نام چیزی که می خواهید بسازید را در جایگزین یاب جستجو کنید و وارد صفحه آن
        شوید. اگر جستجوگر جایگزین یاب آن را به شما پیشنهاد نمی دهد، ابتدا باید
        <a href="how-add-name"> نام آن را به جستجوگر اضافه کنید. </a>
      </li>
      <li>
        روی دکمه مشاهده و ویرایش در فهرست داده های عمومی کلیک کنید.
      </li>
      <li>
        بر روی دکمه ایجاد صفحه کلیک کنید.
      </li>
      <li>
        با توجه به
        <Link to="page-structure"> قالب صفحه </Link>
        صفحه مورد نظر را بسازید.
      </li>
    </ol>
  </InfoWrapper>;
};
