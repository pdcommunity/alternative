import React from "react";
import {
  useRouteMatch,
  Switch,
  Route,
  Link,
} from "react-router-dom";
import { NotFound } from "../../template/NotFound";
import { HowAddPage } from "./HowAddPage";
import { InfoWrapper } from "../InfoWrapper";
import { TemplateInfo } from "./TemplateInfo";

const Menu = () => {
  return <InfoWrapper>
    <ul>
      <li><Link to="help/page-structure">ساختار صفحه</Link></li>
      <li><Link to="help/how-add-page">ساخت صفحه</Link></li>
      <li><Link to="help/how-add-name">ثبت یک اسم در جستجوگر</Link></li>
    </ul>
  </InfoWrapper>;
};

export const HelpRouter = () => {
  const { url } = useRouteMatch();
  return <Switch>
    <Route path={`${url}/how-add-page`}>
      <HowAddPage/>
    </Route>
    <Route path={`${url}/page-structure`}>
      <TemplateInfo/>
    </Route>
    <Route path={`${url}/`} exact>
      <Menu/>
    </Route>
    <Route path="*">
      <NotFound/>
    </Route>
  </Switch>;
};
