import React from "react";
import { Navbar } from "./Navbar";
import { Container } from "react-bootstrap";

export const DefaultTemplate = ({ children }) => {
  const c = '';
  return <div className={c} style={{ backgroundColor: '#000', minHeight: '100vh' }}>
    <Navbar/>
    <div style={{ height: '20px' }}></div>
    <Container>
      {children}
    </Container>
  </div>;
};
