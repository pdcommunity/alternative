import React from "react";
import { DefaultTemplate } from "./DefaultTemplate";

export const NotFound = () => (
  <DefaultTemplate>
    <h1 className="text-white">صفحه مورد نظر شما پیدا نشد</h1>
    <p className="text-white">
    مشکلی وجود دارد؟
    <a href="//pdcommunity.ir/about#contact"> با ما تماس بگیرید. </a>
    </p>
  </DefaultTemplate>
);
