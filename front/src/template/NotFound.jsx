import React from "react";
import { DefaultTemplate } from "./DefaultTemplate";

export const NotFound = () => (
  <DefaultTemplate>
    <h1>صفحه مورد نظر شما پیدا نشد</h1>
  </DefaultTemplate>
);
