import React from "react";
import { Nav, Navbar as BootstrapNavbar } from "react-bootstrap";
import { Link } from "react-router-dom";

export const Navbar = () => (
  <BootstrapNavbar bg="primary" variant="dark">
    <Nav>
      <Nav.Link as="a" href="/">خانه</Nav.Link>
      <Nav.Link as={Link} to="/info/about">درباره این برنامه</Nav.Link>
      <Nav.Link as={Link} to="/info/help">راهنمای مشارکت</Nav.Link>
    </Nav>
  </BootstrapNavbar>
);
