import React from "react";
import { Nav, Navbar as BootstrapNavbar } from "react-bootstrap";
import { Link } from "react-router-dom";

export const Navbar = () => (
  <BootstrapNavbar bg="primary" variant="dark">
    <Nav>
      <Nav.Link as={Link} to="/">خانه</Nav.Link>
      <Nav.Link as={Link} to="/project">پروژه ها</Nav.Link>
    </Nav>
  </BootstrapNavbar>
);
