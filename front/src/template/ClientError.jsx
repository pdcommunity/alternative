import React from "react";
import { DefaultTemplate } from "./DefaultTemplate";

export const ClientError = () => (
  <DefaultTemplate>
    <div className="text-white">
    <h1>خطایی رخ داده است.</h1>
    <p>
      شما نباید این صفحه را ببینید و در حال تجربه یک باگ هستید. اگر مایل هستید می توانید
      این باگ را گزارش کرده و به پیشرفت ما کمک کنید.
    </p>
    </div>
  </DefaultTemplate>
);
