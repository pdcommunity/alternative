/* eslint-disable immutable/no-this */
/* eslint-disable immutable/no-mutation */
import React from "react";
import { ClientError } from "./ClientError";

export class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    console.log(error, info);
  }

  render() {
    if (this.state.hasError) {
      return <ClientError/>;
    }
    return this.props.children;
  }
}
