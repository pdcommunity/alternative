import React, { useState } from "react";
import { FormControl } from "react-bootstrap";
import { CandidateList } from "./CandidateList";
import { DefaultTemplate } from "../template/DefaultTemplate";

export const Home = () => {
  const [q, setQ] = useState('');
  const height = q === '' ? '40vh' : '10px';
  return <DefaultTemplate>
    <div style={{ height }}></div>
    <div className="text-center">
      <h1 className="text-primary" style={{ fontSize: '10vh', whiteSpace: 'nowrap' }}>
        جایگزین یاب
      </h1>
      <FormControl
        placeholder="نام نرم افزار یا داده انحصاری را وارد کنید"
        value={q}
        onChange={(e)=>setQ(e.target.value)}
      />
    </div>
    <br/>
    <CandidateList q={q}/>
  </DefaultTemplate>;
};
