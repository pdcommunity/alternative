import React from "react";
import { Row, Col } from "react-bootstrap";
import { CandidateItem } from "./CandidateItem";
import { search } from "../data/search";

export const CandidateList = (props) => {
  if (props.q === '') return <div/>;
  const result = search(props.q).map(x => <Col key={x} sm={12} md={6} lg={4}>
    <CandidateItem title={x}/>
  </Col>);
  return <div>
    <Row>
      {result}
    </Row>
  </div>;
};
