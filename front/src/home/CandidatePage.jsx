import React from "react";
import { useParams } from "react-router-dom";
import { DefaultTemplate } from "../template/DefaultTemplate";
import { AltItem, AltMessage } from "./page/AltItem";
import {
  Row, Col, Card, Button,
} from "react-bootstrap";
import { useWikiApi } from "../wikiApi/hook";

const Title = ({ title }) => {
  const open = () => {
    const win = window.open('https://index.pdcommunity.ir/wiki/' + title, '_blank');
    win.focus();
  };
  return <h1 className="text-white">
    {title}
    <Button onClick={open} style={{ float: 'left' }}>
      مشاهده و ویرایش در فهرست داده های عمومی
    </Button>
  </h1>;
};

export const CandidatePage = () => {
  const { title } = useParams();
  const data = useWikiApi(title);
  if (data.loading) {
    return <DefaultTemplate>
      <Title title={title}/>
      <p className="text-white">
        در حال بارگیری...
      </p>
    </DefaultTemplate>;
  }
  if (!data.ok) {
    return <DefaultTemplate>
      <Title title={title}/>
      <p className="text-white">
        متاسفانه در فهرست داده های عمومی صفحه ای درباره این داده انحصاری وجود ندارد.
        با ساخت صفحه و افزودن جایگزین ها به جامعه کمک کنید.
      </p>
    </DefaultTemplate>;
  }
  const altPart = (()=> {
    if (data.alt.length === 1 && data.alt[0].redirect) {
      return <AltMessage message={`در حال بارگیری جایگزین ها از ${data.alt[0].to}...`}/>;
    }
    return data.alt.map((x) => <AltItem key={x.name} data={x}/>);
  })();
  return <DefaultTemplate>
    <Title title={title}/>
    <p className="text-white">
      {data.description}
    </p>
    <Row>
      <Col sm={8}>
        {altPart}
      </Col>
      <Col>
        <Card bg="danger">
          <Card.Header>چرا باید {title} را جایگزین کنیم؟</Card.Header>
          <Card.Body>
            <Card.Text>
              {data.whyNot === '' ? 'با افزودن توضیحات بیشتر در این قسمت، به جامعه کمک کنید' : data.whyNot}
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  </DefaultTemplate>;
};
