import React from "react";
import { Card } from "react-bootstrap";

const CommentItem = ({ data }) => {
  const { tag, description } = data;
  const v = tag === 'عیب' ? 'danger' : 'success';
  return <Card bg={v} style={{ margin: '0 0 10px 0' }}>
    <Card.Header>{tag}</Card.Header>
    <Card.Body>
      <Card.Text>{description}</Card.Text>
    </Card.Body>
  </Card>;
};

export const AltItem = ({ data }) => {
  const { name, description, comments } = data;
  return <Card style={{ margin: '0 0 20px 0' }}>
    <Card.Body>
      <Card.Title>{name}</Card.Title>
      <Card.Text>{description}</Card.Text>
      {comments.map((x, i) => <CommentItem key={i} data={x}/>)}
    </Card.Body>
  </Card>;
};
