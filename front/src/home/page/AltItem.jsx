import React from "react";
import { Card, Button } from "react-bootstrap";

const CommentItem = ({ data }) => {
  const { tag, description } = data;
  const v = tag === 'عیب' ? 'danger' : 'success';
  return <Card bg={v} style={{ margin: '0 0 10px 0' }}>
    <Card.Header>{tag}</Card.Header>
    <Card.Body>
      <Card.Text>{description}</Card.Text>
    </Card.Body>
  </Card>;
};

const Title = ({ title }) => {
  const open = () => {
    const win = window.open('https://index.pdcommunity.ir/wiki/' + title, '_blank');
    win.focus();
  };
  return <Card.Title>
    {title}
    <Button onClick={open} style={{ float: 'left' }}>
      مشاهده
    </Button>
  </Card.Title>;
};

export const AltItem = ({ data }) => {
  const { name, description, comments } = data;
  return <Card style={{ margin: '0 0 20px 0' }}>
    <Card.Body>
      <Title title={name}></Title>
      <Card.Text>{description}</Card.Text>
      {comments.map((x, i) => <CommentItem key={i} data={x}/>)}
    </Card.Body>
  </Card>;
};

export const AltMessage = ({ message }) => {
  return <Card style={{ margin: '0 0 20px 0' }}>
    <Card.Body>
      <Card.Title>{message}</Card.Title>
    </Card.Body>
  </Card>;
};
