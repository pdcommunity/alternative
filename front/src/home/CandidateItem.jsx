import React from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { useWikiApi } from "../wikiApi/hook";

export const CandidateItem = ({ title }) => {
  const wikidata = useWikiApi(title);
  return <Card style={{ margin: '0 0 20px 0' }}>
    <Card.Body>
      <Card.Title>{title}</Card.Title>
      <Card.Text>
        {wikidata.description}
      </Card.Text>
      <Button block as={Link} to={`/${title}`}>مشاهده جایگزین ها</Button>
    </Card.Body>
  </Card>;
};
