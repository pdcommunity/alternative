/* eslint-disable no-restricted-globals */
/* eslint-disable toplevel/no-toplevel-side-effect */

console.log('salam');

const version = '1.0.1';
const mainCache = 'main-v1';
const wikiCache = 'wiki-v1';
const assetCache = 'asset-v1';
const expectedCaches = [mainCache, wikiCache, assetCache];

const addToCache = (cn, request, response) => caches.open(cn)
  .then(cache => cache.put(request, response));

self.addEventListener('install', async (event) => {
  event.waitUntil((async () => {
    try {
      await (await caches.open(mainCache)).addAll(['/', '/dist/bundle.js']);
      await (await caches.open(assetCache)).addAll([
        '/dist/bootstrap.css',
        '/dist/Vazir.ttf',
        '/dist/default.png',
        '/dist/manifest.json',
      ]);
      await (await caches.open(wikiCache)).addAll([
        '/dist/data.json',
      ]);
      await self.skipWaiting();
    } catch (e) {
      console.log(e);
      throw e;
    }
  })());
});

self.addEventListener('activate', (event) => {
  event.waitUntil((async () => {
    console.log('message sent');
    const cs = await self.clients.matchAll({
      type: 'window',
    });
    if (cs.length > 0) {
      cs[0].postMessage({
        type: 'activated',
        version,
      });
    }
    const keys = await caches.keys();
    await Promise.all(
      keys.map(async (key) => {
        if (!expectedCaches.includes(key)) {
          await caches.delete(key);
        }
      }),
    );
  })());
});

self.addEventListener('fetch', (event) => {
  const url = new URL(event.request.url);
  if (url.hostname === 'index.pdcommunity.ir') {
    event.respondWith((async ()=>{
      const cachedRes = await caches.match(event.request);
      if (cachedRes) {
        event.waitUntil((async () => {
          await addToCache(wikiCache, event.request, await fetch(event.request));
        })());
        return cachedRes;
      }
      const res = await fetch(event.request);
      if (res.ok) {
        addToCache(wikiCache, event.request, res.clone());
      }
      return res;
    })());
  } else if (url.pathname.startsWith('/dist/')) {
    if (url.pathname === '/dist/data.json') {
      event.waitUntil((async () => {
        await addToCache(wikiCache, event.request, await fetch(event.request));
      })());
      event.respondWith(
        caches.match(event.request),
      );
    } else {
      event.respondWith(
        caches.match(event.request),
      );
    }
  } else {
    event.respondWith(
      caches.match('/'),
    );
  }
});
